<!-- favicon -->
<link rel=icon href="<?php echo base_url();?>assets/master/img/apti.png" sizes="20x20"
	type="image/png">
<!-- animate -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/animate.css">
<!-- bootstrap -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/bootstrap.min.css">
<!-- magnific popup -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/magnific-popup.css">
<!-- owl carousel -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/owl.carousel.min.css">
<!-- fontawesome -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/themify-icons.css">
<!-- flaticon -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/flaticon.css">
<!-- slick slider -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/slick.css">
<!-- animated slider -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/animated-slider.css">
<!-- Main Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/style.css">
<!-- responsive Stylesheet -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/responsive.css">

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Tinos" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/master/css/progress-tracker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/master/css/site.css">
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/master/css/style_manual.css">
 -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/css/style.css">
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/aos/aos.css">
 --><link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-grid.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/icofont/icofont.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-grid.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-grid.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-reboot.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap-reboot.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/boxicons/css/animations.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/boxicons/css/boxicons.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/boxicons/css/boxicons.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>portfolio_assets/vendor/boxicons/css/transformations.css">