
 <!--Edit Daily Members -->
    <div class="row">
    <div class="col-12">
        <h4>Edit Certificate</h4>
        <form class="needs-validation" novalidate="" action="<?php echo base_url('portfolio/u');?>" method="post" enctype="multipart/form-data">
            <div class="card-header">

                <div class="form-group row">
                    <div class="form-group col-md-4">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" required="" value="<?php echo $portfolio['title'];?>">
                        <div class="invalid-feedback">Enter Title?</div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $portfolio['id'] ; ?>">
                       <div class="form-group col-md-4">
                        <label>Client</label>
                        <input type="text" name="client" class="form-control" required="" value="<?php echo $portfolio['client'];?>">
                        <div class="invalid-feedback">Enter Title?</div>
                    </div>
                     <div class="form-group col-md-4">
                        <label>Project Date</label>
                        <input type="date" name="project_date" class="form-control" required="" value="<?php echo $portfolio['project_date'];?>">
                        <div class="invalid-feedback">Enter Title?</div>
                    </div>
                    <div class="form-group col-md-4">
                                <label>Upload Image</label>
                                <input type="file" name="file" class="form-control" onchange="readURL(this);" value="<?php echo base_url(); ?>uploads/portfolio_image/portfolio_<?php echo $portfolio['id']; ?>.jpg?<?php echo time();?>">
                            <br><img src="<?php echo base_url(); ?>uploads/portfolio_image/portfolio_<?php echo $portfolio['id']; ?>.jpg?<?php echo time();?>" style="width: 200px;" />
                                
<!--                                 <input type="file" class="form-control" name="file"> -->
                                <div class="invalid-feedback">Upload Image?</div>
                            </div>
                             <div class="form-group col-md-4">
                        <label>Category</label>
                        <input type="text" name="project_category" class="form-control" required="" value="<?php echo $portfolio['project_category'];?>">
                        <div class="invalid-feedback">Enter Title?</div>
                    </div>
                    <div class="col col-sm col-md" >
<label>Description</label>
            <textarea id="product_desc" name="desc" class="ckeditor" rows="10" data-sample-short> <?php echo $portfolio['desc'];?></textarea>
           <?php echo form_error('desc', '<div style="color:red">', '</div>');?>
         </div>
					 <div class="form-group col-md-12">
                        <button class="btn btn-primary mt-27 ">Update</button>

                    </div>
                </div>
            </div>

        </form>
    </div>
</div>


