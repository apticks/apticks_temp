<!-- breadcrumb area start -->
<div class="breadcrumb-area" style="background-image:url(assets/master/img/page-title-bg.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-inner">
                    <h1 class="page-title">Software Testing</h1>
                    <ul class="page-list">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <li>Software Testing</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content area Start -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<!-- Content area End -->
<div class="container">
        <div class="row">
            <div class="col-lg-6 desktop-center-item">
                <div class="desktop-center-area wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two about">
                     <!--   <h2 class="title about">Software<span>Testing</span></h2> -->
                        <p>Web testing could be a code testing follow to check net sites or web applications for potential bugs. it is a complete testing of web-based applications before creating live.
A web-based system has to be checked fully from finish-to-end before it goes live for end users.
By performing arts web site testing, a company will certify that the web-based system is functioning properly and may be accepted by time period users.
The UI style and practicality area unit the captains of web site testing.</p>
<p>Web Testing Checklists:</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Functionality Testing</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Usability testing</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Interface testing</span>
                            </div>
                             <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Compatibility testing</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Performance testing</span>
                            </div>
                             
                        </div>
                         <div class="col-md-6">
                             
                             <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Security testing</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
             
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/test.gif" alt="video">
                    </div>
                
            
            </div>
            
        </div>
    </div>
    <div class="sbst-service-area pd-top-80">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="section-title text-center about">
                    <h2 class="title">Testing -<span>Automation?</span></h2>
                    <p class="section-title about"><span >APTICKS</span> providing for changing market demands make it challenging for enterprises to convey fantastic programming at fast. Conventional programming testing approaches, including manual testing, are insufficient to quickly convey programming with flawless quality.Thus, test automation has picked up noticeable quality within the recent times.

Our test automation is backed by advancement center points for automation arrangements supported new tools and technologies. We make and use a couple of restrictive structures and technologies that are meaningful for your test automation objectives and fit your budget requirements.

These tools and frameworks are industry-specific and mostly open source based.Our solution is driven by automation objectives and not by tool ability. Our test automation services are deliberately flexible and enhanced around all the phases of development life cycle.
</p>
                </div>
            </div>
        </div>
<style type="text/css">
    ul.a {list-style-type: none;}
</style>


