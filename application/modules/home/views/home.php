<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css">
<meta name="viewport" content="width=device-width">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js"></script>


<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background: rgb(2,0,36);
background: radial-gradient(circle, rgba(2,0,36,1) 0%, rgba(100,231,133,1) 0%, rgba(127,126,205,1) 100%, rgba(0,212,255,1) 100%);">
        <img style="height: 500px;padding-left: 31%" src="<?php echo base_url();?>assets/img/MOBILEDEVELOPMENT.png"/>
       
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background: radial-gradient(circle, rgba(2,0,36,1) 0%, rgba(233,228,228,1) 0%, rgba(0,0,4,1) 100%, rgba(0,212,255,1) 100%);">
       <img style="height: 500px;" src="<?php echo base_url();?>assets/img/outsource.png"/>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background: rgb(2,0,36);
background: radial-gradient(circle, rgba(2,0,36,1) 0%, rgba(100,231,133,1) 0%, rgba(127,126,205,1) 100%, rgba(0,212,255,1) 100%);">
      <img style="height: 500px;padding-left:44% " src="<?php echo base_url();?>assets/img/WebDevlopement_png1.png"/>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
</header>

<!-- Page Content -->

<!-- header area End -->
  <meta name="viewport" content="width=device-width, initial-scale=1">   
<div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">What We <span>Do?</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">We believe in building strong brands, good clean design, well-crafted content, and integrated strategies.</p>
                </div>
            </div>
        </div>
      

<!--/.Carousel Wrapper-->
<body class="hm-gradient">

    <main>
        
        <!--MDB Cards-->
        <div class="container mt-4">

            <!-- Grid row -->
            <div class="row">
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-12 mb-4">
                    
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/software.png" class="img-fluid" alt="photo">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title">Software Development</h4>
                            <!--Text-->
                            <p class="card-text">Software developer, also referred to as a programmer , you’ll be playing a key role within the design, installation, testing and maintenance of software systems. The creative minds behind computer programs.</p>
                            <a href="#" class="btn btn-deep-orange btn-md">Read more</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-4">
            
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/MOBILEDEVELOPMENT.png" class="img-fluid" alt="photo" style="height: 274px;">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body text-center">
                            <!--Title-->
                            <h4 class="card-title indigo-text"><strong>Mobile application</strong></h4>
                            <!--Text-->
                            <p class="card-text">APTICKS Mobile application are custom-manufactured, compact arrangements and administrations to new companies by utilizing our times of experience delivering astounding advanced items.</p>
                            <a href="#" class="btn btn-indigo btn-md">Learn more</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-4">
            
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/WebDevlopement_png1.png" class="img-fluid" alt="photo" style="height: 323px;">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body text-center">
                            <!--Title-->
                            <h4 class="card-title font-up brown-text">Web Development</h4>
                            <!--Text-->
                            <p class="card-text black-text">Web application development can generally have a brief development life-cycle lead by a little development team. </p>
                            <a href="#" class="btn btn-brown btn-md">More content</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
            </div>
            <!-- Grid row -->
            <div class="row">
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-12 mb-4">
                    
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/DigitalMarketing.png" class="img-fluid" alt="photo">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <h4 class="card-title">Digital Marketing</h4>
                            <!--Text-->
                            <p class="card-text">Digital promoting relies on technology that is ever-evolving and fast-changing, a similar options to be expected from digital promoting developments and methods. APTICKS marketing  will help you grow your business.</p>
                            <a href="#" class="btn btn-deep-orange btn-md">Read more</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-4">
            
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/GraphicDesign.png" class="img-fluid" alt="photo" style="height: 259px;">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body text-center">
                            <!--Title-->
                            <h4 class="card-title indigo-text"><strong>Graphics Design</strong></h4>
                            <!--Text-->
                            <p class="card-text">APTICKS a years of multi tasking and multi disciplined design studio that gives strength to strength fresh concepts encompassing dedicated service levels, meeting all the creative needs of clients and organizations in India and overseas.</p>
                            <a href="#" class="btn btn-indigo btn-md">Learn more</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-lg-4 col-md-6 mb-4">
            
                    <!--Card-->
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="<?php echo base_url();?>assets/img/test.png" class="img-fluid" alt="photo" style="height: 282px;">
                            <a href="#!">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body text-center">
                            <!--Title-->
                            <h4 class="card-title font-up brown-text">Software Testing</h4>
                            <!--Text-->
                            <p class="card-text black-text">APTICKS providing platform for changing market demands  to convey fantastic programming at fast. Web testing could be a code testing follow to check net sites or web applications for potential bugs.</p>
                            <a href="#" class="btn btn-brown btn-md">More content</a>
                        </div>

                    </div>
                    <!--/.Card-->
            
                </div>
                <!-- Grid column -->
            
            </div>
            
      
    </main>
  
</body>
<!-- sbst service area end -->

<div class="sbst-provide-security mg-top-100 mg-bottom-120" style="background-image:url(<?php echo base_url();?>assets/master/img/startup/why-choose/2.png);">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6">
                <div class="img-with-video img-with-video-2 wow animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="img-wrap">
                        <img src="<?php echo base_url();?>assets/master/img/startup/why-choose/1.png" alt="video">
                        <div class="hover">
                            <!-- <a href="https://www.youtube.com/watch?v=c7XEhXZ_rsk" class="video-play-btn mfp-iframe"><img src="<?php //echo base_url();?>assets/master/img/we-provide/3.png" alt="video"></a> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="wow animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                        <h2 class="title">Why Choose <span>APTICKS?</span></h2>
                        <p>APTICKS One of the best Software Development company. Our co-creation framework and technology  guides choice of right technologies and architecture and builds future-ready products. From concept design to development, testing & implementation, our team is here to support and guide you every step of the way.</p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Team Work</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Concept Implementation</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Good Job Invironment</span>
                            </div>
                            <div class="riyaqas-check-list">
                                <img src="<?php echo base_url();?>assets/master/img/icons/check.png" alt="check">
                                <span>Work Freedom</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sbst-skill-area pd-top-120">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6 desktop-center-item">
                <div class="animated fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="section-title style-two">
                        <h2 class="title">We Have Strong <span>Skill</span></h2>
                        <p>APTICKS is at the most innovative web development company. Our cost efficient and structured teamwork justifies.</p>
                    </div>
                    <div class="all-progress" id="progress-running">
                        <div class="progress-item">
                            <span class="progress-heading">User Interface Design</span>
                            <span class="progress-count">95%</span>
                            <div  class="progress-bg">
                                <div id='progress-one' class="progress-rate" data-value='95'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item">
                            <span class="progress-heading">User Experience</span>
                            <span class="progress-count">86%</span>
                            <div  class="progress-bg">
                                <div id='progress-two' class="progress-rate" data-value='86'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item">
                            <span class="progress-heading">Dymanic Programming Solutions</span>
                            <span class="progress-count">90%</span>
                            <div  class="progress-bg">
                                <div id='progress-three' class="progress-rate" data-value='90'>
                                </div>
                            </div>
                        </div>
                        <div class="progress-item mg-bottom-0-991">
                            <span class="progress-heading">Market Analysis</span>
                            <span class="progress-count">80%</span>
                            <div  class="progress-bg">
                                <div id='progress-four' class="progress-rate" data-value='80'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 offset-xl-1 col-lg-6">
                <div class="shape-bg-image animated fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="thumb">
                        <img src="<?php echo base_url();?>assets/master/img/startup/skill/1.png" alt="service">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- offer area start -->
<div class="sbst-offer-area pd-top-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">What We are <span>Offering?</span></h2>
                    <p class="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">We want to determine an open and honest partnership with each customer that uses our services. We try to introduce third-party services, new code tools and management solutions.</p>
                </div>
            </div>
        </div>
        <div class="row custom-gutters-28 justify-content-center">
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/1.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Challenging Project</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.3s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/2.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Competitive salary</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/3.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Play Zone</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.7s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/4.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Strong Team</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.8s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/5.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Decorated Office</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-4 col-sm-6">
                <div class="single-offer wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="1.0s">
                    <div class="media">
                        <img src="<?php echo base_url();?>assets/master/img/startup/offer/6.png" alt="offer">
                        <div class="media-body align-self-center">
                            <h6>Free Coffee &amp; Snacks</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- offer area End -->
<!-- client area start -->
<div class="sbst-client-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Our valuable <span>Client</span></h2>
                    <p>The relationship between a manufacturer and his advertising agency is almost as intimate as the relationship between a patient and his doctor. Make sure that you can live happily with your prospective client before you accept his account.
</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- client area End -->
<!-- testimonial area Start -->
<div class="testimonial-section sbs-testimonial-section pd-top-105" style="background-image:url(assets/img/bg/1h3.png);">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 order-lg-12 align-self-center ">
                <div class="section-title style-two mb-0">
                    <h2 class="title">What Our <span>Client Say?</span></h2>
                    <p>We are very fortunate to have formed excellent partnerships with many of our clients. And we’ve formed more than just working relationships with them; we have formed true friendships. Here’s what they’re saying about us.</p>
                </div>
            </div>
            <div class="col-lg-6 order-lg-1">
                <div class="sbs-testimonial-slider">
                    <div class="choose_slider">
                        <div class="choose_slider_items">
                            <ul id="testimonial-slider" >
                                <li class="current_item">
                                    <div class="media">
                                     <img src="<?php echo base_url();?>assets/master/img/startup/client/nextclick.jpg" alt="client">
                                        <div class="media-body">
                                            <h6>Vinaysagar</h6>
                                            <p class="designation" >Founder at NextClick</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div> 
                                    <p><a href="<?php echo base_url();?>portfolio">
In my history of working with trade show vendors, I can honestly say that there is not one company that I've ever worked with that has better service than <span>Apticks</span>.</a></p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img src="<?php echo base_url();?>assets/master/img/startup/client/clientfirst.png" alt="client">
                                        <div class="media-body">
                                            <h6>Refule Mia</h6>
                                            <p class="designation">Founder at Dialmama</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>I wanted to follow up on feedback from the Pure Fishing brand and sales teams on the excellent work that you and your team did at the Classic. Your team’s professionalism and willingness to do whatever it takes with minimal interruption was recognized by everyone. My hats off to you for an outstanding job.</p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img  src="<?php echo base_url();?>assets/master/img/startup/client/tradbea.jpg" alt="client">
                                        <div class="media-body">
                                            <h6>YashRaj Sahu</h6>
                                            <p class="designation">CEO at TradeBea</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p><span>Apticks</span> is always responsive to any question I have, they keep me informed and they understand who we are and what we’re trying to do. I firmly believe that. And, any time that I make contact with them, they let me know they’ve received it and they let me know what the next step is.</p>
                                </li>
                                <li class="current_item">
                                    <div class="media">
                                        <img  src="<?php echo base_url();?>assets/master/img/startup/client/clientfirst.png" alt="client">
                                        <div class="media-body">
                                            <h6>John Mojika</h6>
                                            <p class="designation">CEO of Dialmama</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>I wanted to follow up on feedback from the Pure Fishing brand and sales teams on the excellent work that you and your team did at the Classic. Your team’s professionalism and willingness to do whatever it takes with minimal interruption was recognized by everyone. My hats off to you for an outstanding job.</p>
                                </li>
                                
                                <li class="current_item">
                                    <div class="media">
                                        <img src="<?php echo base_url();?>assets/master/img/startup/client/limpex_logo.png" alt="client">
                                        <div class="media-body">
                                            <h6>Dr. Ravi Teja Verma</h6>
                                            <p class="designation">CEO of Limpex</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p>I’m never really worried that the booth isn’t going to get there on time. I can call over there and talk to anyone and get my questions answered in an instant. Ease of getting a hold of people. All communication with them is really easy. They do handle everything.</p>
                                </li>
                                  <li class="current_item">
                                    <div class="media">
                                        <img  src="<?php echo base_url();?>assets/master/img/startup/client/tradbea.jpg" alt="client">
                                        <div class="media-body">
                                            <h6>YashRaj Sahu</h6>
                                            <p class="designation">CEO at TradeBea</p>
                                            <span>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <p><span>Apticks</span> is always responsive to any question I have, they keep me informed and they understand who we are and what we’re trying to do. I firmly believe that. And, any time that I make contact with them, they let me know they’ve received it and they let me know what the next step is.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sbs-arrowleft"><a id="btn_next" href="#"><i class="fa fa-long-arrow-left"></i></a></div>
                    <div class="sbs-arrowright"><a id="btn_prev" href="#"><i class="fa fa-long-arrow-right"></i></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- testimonial area End -->
<!-- testimonial area start -->
<!-- <div class="sbst-testimonial-area mg-top-110">
    <div class="container">
        <div class="testimonial-slider2-bg">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="testimonial-slider-2">
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/2.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>Mr.Mehar</h6>
                                    <span>CEO at Uxa</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/3.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>Wilson jaq</h6>
                                    <span>CEO at kst</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="media text-center">
                                <div class="media-left">
                                    <img src="<?php echo base_url();?>assets/master/img/business/testimonial/4.png" alt="client">
                                </div>
                                <div class="media-body">
                                    <p>Our support team will get assistance from AI-powered suggestions, making it quicker than ever to handle support requests.</p>
                                    <h6>R. Jenkins</h6>
                                    <span>Founder of Uxa</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- testimonial area End 
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag 

<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Project counter</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp'>
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="css/style.css">

  
</head>

<body>

  <div class="sectiontitle">
    <h2>Projects statistics</h2>
    <span class="headerLine"></span>
</div>
<div id="projectFacts" class="sectionClass">
    <div class="fullWidth eight columns">
        <div class="projectFactsWrap ">
            <div class="item wow fadeInUpBig animated animated" data-number="12" style="visibility: visible;">
                <i class="fa fa-briefcase"></i>
                <p id="number1" class="number">12</p>
                <span></span>
                <p>Projects done</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="55" style="visibility: visible;">
                <i class="fa fa-smile-o"></i>
                <p id="number2" class="number">55</p>
                <span></span>
                <p>Happy clients</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="359" style="visibility: visible;">
                <i class="fa fa-coffee"></i>
                <p id="number3" class="number">359</p>
                <span></span>
                <p>Cups of coffee</p>
            </div>
            <div class="item wow fadeInUpBig animated animated" data-number="246" style="visibility: visible;">
                <i class="fa fa-camera"></i>
                <p id="number4" class="number">246</p>
                <span></span>
                <p>Photos taken</p>
            </div>
        </div>
    </div>
</div>


  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  

    <script  src="js/index.js"></script>




</body>

</html>

<style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=PT+Sans+Narrow);
body {
  font-family: Open Sans, "Helvetica Neue", "Helvetica", Helvetica, Arial,   sans-serif;
  font-size: 13px;
  color: black;
  position: relative;
  -webkit-font-smoothing: antialiased;
  margin: 0;
}

* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, p, blockquote, th, td {
  margin: 0;
  padding: 0;
  font-size: 13px;
  direction: ltr;
}

.sectionClass {
  padding: 20px 0px 50px 0px;
  position: relative;
  display: block;
}

.fullWidth {
  width: 100% !important;
  display: table;
  float: none;
  padding: 0;
  min-height: 1px;
  height: 100%;
  position: relative;
}


.sectiontitle {
  background-position: center;
  margin: 30px 0 0px;
  text-align: center;
  min-height: 20px;
}

.sectiontitle h2 {
  font-size: 30px;
  color: #222;
  margin-bottom: 0px;
  padding-right: 10px;
  padding-left: 10px;
}


.headerLine {
  width: 160px;
  height: 2px;
  display: inline-block;
  background: #101F2E;
}


.projectFactsWrap{
    display: flex;
  margin-top: 30px;
  flex-direction: row;
  flex-wrap: wrap;
}


#projectFacts .fullWidth{
  padding: 0;
}

.projectFactsWrap .item{
  width: 25%;
  height: 100%;
  padding: 50px 0px;
  text-align: center;
}

.projectFactsWrap .item:nth-child(1){
  background: rgb(222, 27, 27);

}

.projectFactsWrap .item:nth-child(2){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item:nth-child(3){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item:nth-child(4){
  background: rgb(222, 27, 27);
}

.projectFactsWrap .item p.number{
  font-size: 40px;
  padding: 0;
  font-weight: bold;
}

.projectFactsWrap .item p{
  color: rgba(23, 21, 21, 0.8);
  font-size: 18px;
  margin: 0;
  padding: 10px;
  font-family: 'Open Sans';
}


.projectFactsWrap .item span{
  width: 60px;
  background: rgba(255, 255, 255, 0.8);
  height: 2px;
  display: block;
  margin: 0 auto;
}


.projectFactsWrap .item i{
  vertical-align: middle;
  font-size: 50px;
  color: rgba(23, 21, 21, 0.8);
}


.projectFactsWrap .item:hover i, .projectFactsWrap .item:hover p{
  color: white;
}

.projectFactsWrap .item:hover span{
  background: white;
}

@media (max-width: 786px){
  .projectFactsWrap .item {
     flex: 0 0 50%;
  }
}

/* AUTHOR LINK */


footer{
  z-index: 100;
  padding-top: 50px;
  padding-bottom: 50px;
  width: 100%;
  bottom: 0;
  left: 0;
}

footer p {
color: rgba(255, 255, 255, 0.8);
  font-size: 16px;
  opacity: 0;
  font-family: 'Open Sans';
  width: 100%;
    word-wrap: break-word;
  line-height: 25px;
  -webkit-transform: translateX(-200px);
  transform: translateX(-200px);
  margin: 0;
  -webkit-transition: all 250ms ease;
  -moz-transition: all 250ms ease;
  transition: all 250ms ease;
}

footer .authorWindow a{
  color: white;
  text-decoration: none;
}

footer p strong {
    color: rgba(255, 255, 255, 0.9);
}

.about-me-img {
  width: 120px;
  height: 120px;
  left: 10px;
  /* bottom: 30px; */
  position: relative;
  border-radius: 100px;
}


.about-me-img img {
}


.authorWindow{
  width: 600px;
  background: #75439a;
  padding: 22px 20px 22px 20px;
  border-radius: 5px;
  overflow: hidden;
}

.authorWindowWrapper{
  display: none;
  left: 110px;
  top: 0;
  padding-left: 25px;
  position: absolute;
}





.trans{
  opacity: 1;
  -webkit-transform: translateX(0px);
  transform: translateX(0px);
  -webkit-transition: all 500ms ease;
  -moz-transition: all 500ms ease;
  transition: all 500ms ease;
}

@media screen and (max-width: 768px) {
    .authorWindow{
         width: 210px;
    }

    .authorWindowWrapper{
             bottom: -170px;
  margin-bottom: 20px;
    }

    footer p{
          font-size: 14px;
    }
}



</style><script type="text/javascript">
        $.fn.jQuerySimpleCounter = function( options ) {
        var settings = $.extend({
            start:  0,
            end:    100,
            easing: 'swing',
            duration: 400,
            complete: ''
        }, options );

        var thisElement = $(this);

        $({count: settings.start}).animate({count: settings.end}, {
            duration: settings.duration,
            easing: settings.easing,
            step: function() {
                var mathCount = Math.ceil(this.count);
                thisElement.text(mathCount);
            },
            complete: settings.complete
        });
    };


$('#number1').jQuerySimpleCounter({end: 12,duration: 3000});
$('#number2').jQuerySimpleCounter({end: 55,duration: 3000});
$('#number3').jQuerySimpleCounter({end: 359,duration: 2000});
$('#number4').jQuerySimpleCounter({end: 246,duration: 2500});
</script>--->
<style type="text/css">.about{
    font-family: tinos;
    font-size: 20px;
   } 
   </style>
   <!--new counter---->
   <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
<div class="sbst-client-area pd-top-112">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-lg-10">
                <div class="section-title text-center">
                    <h2 class="title">Our <span>Statistics</span></h2>
                </div>
            </div>
        </div>
       
    </div>
</div>
<div class="container">
        <div class="row text-center">
            <div class="col">
            <div class="counter">
      <i class="fa fa-code fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="5" data-speed="1500"></h2>
       <p class="count-text ">Our <span>Customer</span></p>
    </div>
            </div>
              <div class="col">
               <div class="counter">
      <i class="fa fa-coffee fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="5" data-speed="1500"></h2>
      <p class="count-text ">Happy Clients</p>
    </div>
              </div>
              <div class="col">
                  <div class="counter">
      <i class="fa fa-lightbulb-o fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="1" data-speed="1500"></h2>
      <p class="count-text ">Projects Completed</p>
    </div></div>
              <div class="col">
              <div class="counter">
      <i class="fa fa-bug fa-2x"></i>
      <h2 class="timer count-title count-number" data-to="50" data-speed="1500"></h2>
      <p class="count-text ">Coffee With Clients</p>
    </div>
              </div>
         </div>
</div>

<style type="text/css">
    
    .counter {
    background-color:#f5f5f5;
    padding: 20px 0;
    border-radius: 5px;
}

.count-title {
    font-size: 40px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
  
    
}

.count-text {
    font-size: 13px;
    font-weight: normal;
    margin-top: 10px;
    margin-bottom: 0;
    text-align: center;
    color: #e41f0b;

}

.fa-2x {
    margin: 0 auto;
    float: none;
    display: table;
    color: #e41f0b;
}
.go{
    color: #fff;
    background-color: #d20523;
    border-color: #080808;
}
.carousel-item {
  height: 100vh;
  min-height: 350px;
  background: no-repeat center center scroll;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
<script type="text/javascript">
    (function ($) {
    $.fn.countTo = function (options) {
        options = options || {};
        
        return $(this).each(function () {
            // set options for current element
            var settings = $.extend({}, $.fn.countTo.defaults, {
                from:            $(this).data('from'),
                to:              $(this).data('to'),
                speed:           $(this).data('speed'),
                refreshInterval: $(this).data('refresh-interval'),
                decimals:        $(this).data('decimals')
            }, options);
            
            // how many times to update the value, and how much to increment the value on each update
            var loops = Math.ceil(settings.speed / settings.refreshInterval),
                increment = (settings.to - settings.from) / loops;
            
            // references & variables that will change with each update
            var self = this,
                $self = $(this),
                loopCount = 0,
                value = settings.from,
                data = $self.data('countTo') || {};
            
            $self.data('countTo', data);
            
            // if an existing interval can be found, clear it first
            if (data.interval) {
                clearInterval(data.interval);
            }
            data.interval = setInterval(updateTimer, settings.refreshInterval);
            
            // initialize the element with the starting value
            render(value);
            
            function updateTimer() {
                value += increment;
                loopCount++;
                
                render(value);
                
                if (typeof(settings.onUpdate) == 'function') {
                    settings.onUpdate.call(self, value);
                }
                
                if (loopCount >= loops) {
                    // remove the interval
                    $self.removeData('countTo');
                    clearInterval(data.interval);
                    value = settings.to;
                    
                    if (typeof(settings.onComplete) == 'function') {
                        settings.onComplete.call(self, value);
                    }
                }
            }
            
            function render(value) {
                var formattedValue = settings.formatter.call(self, value, settings);
                $self.html(formattedValue);
            }
        });
    };
    
    $.fn.countTo.defaults = {
        from: 0,               // the number the element should start at
        to: 0,                 // the number the element should end at
        speed: 1000,           // how long it should take to count between the target numbers
        refreshInterval: 100,  // how often the element should be updated
        decimals: 0,           // the number of decimal places to show
        formatter: formatter,  // handler for formatting the value before rendering
        onUpdate: null,        // callback method for every time the element is updated
        onComplete: null       // callback method for when the element finishes updating
    };
    
    function formatter(value, settings) {
        return value.toFixed(settings.decimals);
    }
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
    formatter: function (value, options) {
      return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
    }
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
  }
});
document.querySelector(".card-flip").classList.toggle("flip");

/* 
 * Holder.js for demo image
 * Just for demo purpose
 */
Holder.addTheme('gray', {
  bg: '#777',
  fg: 'rgba(255,255,255,.75)',
  font: 'Helvetica',
  fontweight: 'normal'
});
</script>


<style type="text/css">
  

/*.hm-gradient {
    background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
}
.darken-grey-text {
    color: #2E2E2E;
}*/
.img-fluid{
width: 375px;
    height: 250px;
}

</style>  

